import 'babel-polyfill';
import Vue from 'vue'
import store from './store/store'
import App from './components/master/Master.vue'

Vue.config.productionTip = false

new Vue({
  el: '#app',
  store,
  render: h => h(App),
  created() {
    store.dispatch("init");
  }
})
