const jsonfile   = require('jsonfile');
const fs         = require('fs');
const express    = require('express');
const bodyParser = require('body-parser');
const cors       = require('cors');
const http       = require('http');
const app        = express();
const port       = 9000;

app.use(cors());

app.use(bodyParser.json());

// get filenames in various folders
app.get('/fileNames', function (request, response) {
  response.setHeader('content-type', 'application/json');
  // create object to hold all component names
  var components = {};
  // go through all content component categories
  for (var category of fs.readdirSync('public/components')) {
    // populate component object with component names for category
    components[category] = fs.readdirSync('public/components/' + category).reduce((acc, name) => {
      // add component name with .vue removed
      acc[name.split('.')[0]] = {};
      return acc;
    }, {});
  }
  // create array to hold all image names
  var images = fs.readdirSync('public/images');
  // create array to hold all font names
  var fonts = fs.readdirSync('public/fonts');
  // respond with file names
  response.status(200).end(JSON.stringify({
    message: "received filenames in various folders",
    components,
    images,
    fonts,
  }));
})

// write changes to json file
app.post('/saveChanges', function (request, response) {
  response.setHeader('content-type', 'application/json');
  jsonfile.writeFile("public/data/website.json", request.body, {spaces: 2}, function (error) {
    if(error) {
      // error writing to json file
      response.status(500).end(JSON.stringify({
        message: "error writing to json file",
        error
      }));
    } else {
      // changes successfully saved to json file
      response.status(200).end(JSON.stringify({
        message: "changes successfully saved to json file"
      }));
    }
  });
})

http.createServer(app).listen(port);
