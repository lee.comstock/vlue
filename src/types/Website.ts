import { Container } from './Container'
import { MobileMenu } from './MobileMenu'
import { GlobalComponentPropValues } from './GlobalComponentPropValues'

export type Website = {
  pageContainers: Container[],
  globalComponentPropValues: GlobalComponentPropValues,
  mobileMenu: MobileMenu,
  fixedLayerContainer: Container,
  baseLayerContainer: Container,
}
