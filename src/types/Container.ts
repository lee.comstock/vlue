import { ContentComponent } from './ContentComponent'

export type Container = {
  childContainers?: Container[],
  pageName?: string,
  parentPageName?: string,
  contentComponents?: ContentComponent[],
  pageContainers?: boolean,
  selected?: boolean,
}
