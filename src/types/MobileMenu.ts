export type MobileMenu = {
  align: string,
  type: string,
  colorA: string,
  colorB: string,
}
