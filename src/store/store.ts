import Vue from 'vue'
import Vuex from 'vuex'
import $ from 'jquery'
import jszip from 'jszip'
import jszipUtils from 'jszip-utils'
import fileSaver from 'file-saver'
import beautify from 'js-beautify'
import cleanSlateJson from '../../public/data/cleanSlate.json'
import { Website } from '../types/Website'
import { Container } from '../types/Container'

Vue.use(Vuex)
const store = new Vuex.Store({
  state: {
    // boolean which tells if you are currently in dev mode
    dev: true,
    // create edit delete
    mode: "create",
    // set to true when downloading generated website
    websiteGeneration: false,
    // used for generating each page as its own html file
    websiteGenerationPage: undefined as string | undefined,
    // used for indicating that website generation is in progress
    websiteGenerationSpinner: false,
    // the layer that is currently selected for editing, 'base' or 'fixed'
    selectedLayer: "base",
    // booleans which tell which area of the website you are editing
    editFrame: true,
    editPage: true,
    // layer visibility, true or false for 'base' and 'fixed'
    layerVisibility: {
      fixed: true,
      base: true,
    },
    // the current page
    currentPage: '',
    // array holding names of images in images folder and uploaded image blob urls
    images: [
      "images/Card.png",
      "images/DesertRock.png",
      "images/DragonAndKnightChickColored.png",
      "images/FranswanCactusettePaintedTransparent.png",
      "images/GhostlyCodeMonkey.png",
      "images/JokerNurseRender.png",
      "images/MagicBottle.png",
      "images/SnakeAndEgg.png",
      "images/TombstoneIslandRendered.png",
      "images/VolcanicPenguinToonPaintedForDeviantArt.png",
    ] as string[],
    // array holding names of fonts in fonts folder
    fonts: [
      "Abel-Pro-Bold.otf",
      "Abel-Pro.otf",
      "AbrilFatface-Regular.ttf",
      "Alata-Regular.ttf",
      "AlegreyaSansSC-Black.ttf",
      "AlegreyaSansSC-BlackItalic.ttf",
      "AlegreyaSansSC-Italic.ttf",
      "AlegreyaSansSC-Regular.ttf",
      "AlegreyaSansSC-Thin.ttf",
      "AlegreyaSansSC-ThinItalic.ttf",
      "AlegreyaSC-Black.ttf",
      "AlegreyaSC-BlackItalic.ttf",
      "AlegreyaSC-Italic.ttf",
      "AlegreyaSC-Medium.ttf",
      "AlegreyaSC-MediumItalic.ttf",
      "Average-Regular.ttf",
      "BarlowCondensed-Bold.ttf",
      "BarlowCondensed-BoldItalic.ttf",
      "BarlowCondensed-ExtraLight.ttf",
      "BarlowCondensed-ExtraLightItalic.ttf",
      "BarlowCondensed-Italic.ttf",
      "BarlowCondensed-Medium.ttf",
      "BarlowCondensed-MediumItalic.ttf",
      "BarlowCondensed-Thin.ttf",
      "BarlowCondensed-ThinItalic.ttf",
      "BebasNeue-Regular.ttf",
      "BerkshireSwash-Regular.ttf",
      "BubblegumSans-Regular.ttf",
      "CarterOne-Regular.ttf",
      "ConcertOne-Regular.ttf",
      "Copse-Regular.ttf",
      "CrimsonText-Bold.ttf",
      "CrimsonText-BoldItalic.ttf",
      "CrimsonText-Italic.ttf",
      "CrimsonText-Regular.ttf",
      "DMSerifText-Italic.ttf",
      "DMSerifText-Regular.ttf",
      "Economica-Bold.ttf",
      "Economica-BoldItalic.ttf",
      "Economica-Italic.ttf",
      "Economica-Regular.ttf",
      "FredokaOne-Regular.ttf",
      "Itim-Regular.ttf",
      "Jomhuria-Regular.ttf",
      "JosefinSans-Bold.ttf",
      "JosefinSans-BoldItalic.ttf",
      "JosefinSans-ExtraLight.ttf",
      "JosefinSans-ExtraLightItalic.ttf",
      "JosefinSans-Regular.ttf",
      "JosefinSans-Thin.ttf",
      "JosefinSans-ThinItalic.ttf",
      "Jost-Black.ttf",
      "Jost-BlackItalic.ttf",
      "Jost-ExtraLight.ttf",
      "Jost-ExtraLightItalic.ttf",
      "Jost-Italic.ttf",
      "Jost-Regular.ttf",
      "Jost-Thin.ttf",
      "Jost-ThinItalic.ttf",
      "LilitaOne-Regular.ttf",
      "Lusitana-Bold.ttf",
      "Lusitana-Regular.ttf",
      "Mali-Bold.ttf",
      "Mali-BoldItalic.ttf",
      "Mali-ExtraLight.ttf",
      "Mali-ExtraLightItalic.ttf",
      "Mali-LightItalic.ttf",
      "Mali-Medium.ttf",
      "Mali-MediumItalic.ttf",
      "Mali-Regular.ttf",
      "MarkaziText-Bold.ttf",
      "MarkaziText-Regular.ttf",
      "MedulaOne-Regular.ttf",
      "OleoScript-Bold.ttf",
      "OleoScript-Regular.ttf",
      "Pacifico-Regular.ttf",
      "Pompiere-Regular.ttf",
      "Prata-Regular.ttf",
      "PTSans-Bold.ttf",
      "PTSans-BoldItalic.ttf",
      "PTSans-Italic.ttf",
      "PTSans-Regular.ttf",
      "PTSansNarrow-Bold.ttf",
      "PTSansNarrow-Regular.ttf",
      "PTSerif-Bold.ttf",
      "PTSerif-BoldItalic.ttf",
      "PTSerif-Italic.ttf",
      "PTSerif-Regular.ttf",
      "Quicksand-Bold.ttf",
      "Quicksand-Light.ttf",
      "Quicksand-Regular.ttf",
      "SourceSansPro-Black.ttf",
      "SourceSansPro-BlackItalic.ttf",
      "SourceSansPro-ExtraLight.ttf",
      "SourceSansPro-ExtraLightItalic.ttf",
      "SourceSansPro-Light.ttf",
      "SourceSansPro-LightItalic.ttf",
      "SourceSansPro-Regular.ttf",
    ] as string[],
    // when populated with properties the create popup is displayed
    createPopupParams: {},
    // when populated with properties the image select is displayed
    imageSelectParams: {},
    // when populated with properties the font select is displayed
    fontSelectParams: {},
    // selected vue component template to download in the sidebar
    selectedComponentTemplate: undefined as string | undefined,
    // stores content component folders, filenames and property types
    contentComponentProps: {
      standard: {
        NavMenu:       {},
        Image:         {},
        ImageWithText: {},
        Header1:       {},
        Header2:       {},
        Header3:       {},
        Header4:       {},
        Header5:       {},
        Header6:       {},
        Paragraph:     {},
      },
    },
    // stores available props for containers with type information
    containerProps: [
      {
        name: 'align',
        type: 'align',
      },
      {
        name: 'size',
        type: 'textfield',
      },
      {
        name: 'gridCellSize',
        type: 'textfield',
      },
      {
        name: 'cssClass',
        type: 'textfield',
      },
      {
        name: 'bgcolor',
        type: 'color',
      },
      {
        name: 'bgImage',
        type: 'image',
      },
      {
        name: 'margin',
        type: 'textfield',
      },
      {
        name: 'padding',
        type: 'textfield',
      },
      {
        name: 'borderRadius',
        type: 'textfield',
      },
      {
        name: 'borderColor',
        type: 'color',
      },
      {
        name: 'borderWidth',
        type: 'textfield',
      },
      {
        name: 'borderStyle',
        type: 'dropdown',
        options: [
          '',
          'solid',
          'dashed',
          'dotted',
        ],
      },
      {
        name: 'shadowSize',
        type: 'textfield',
      },
      {
        name: 'hideOnMobile',
        type: 'flag',
      },
      {
        name: 'disableContentWrap',
        type: 'flag',
      },
    ],
    // stores development viewport width, no value will fit to browser
    viewportWidth: '',
    // stores if website aspect ratio is portrait or landscape
    portraitNotLandscape: false,
    // reference to selected container
    selectedContainer: undefined as (Container | undefined),
    // reference to selected container vue object, used to access parent container
    selectedContainerVueObject: undefined as ({ $parent: any } | undefined),
    // stores website data from json file
    website: {} as Website,
    // stores website states in arrays for undo redo functionality, pop and push states
    websiteHistoryUndo: [] as Website[],
    websiteHistoryRedo: [] as Website[],
    // display error message popup with error message if not undefined
    errorMessage: undefined as {
      title: string,
      description: string,
    } | undefined,
  },
  actions: {
    init(context) {
      context.commit("getWebsiteJson");
      context.commit("loadFontsIntoWebsite");
    },
    undo(context) {
      context.commit("storeStateInHistoryRedo");
      context.commit("undo");
    },
    redo(context) {
      context.commit("storeStateInHistoryUndo", false);
      context.commit("redo");
    },
    changeWebsiteState(context, { mutation, params }) {
      context.commit("storeStateInHistoryUndo", true);
      context.commit(mutation, { ...params, getters: context.getters });
    },
  },
  mutations: {
    storeStateInHistoryUndo(state, emptyRedo) {
      // unselect selected container to avoid issues
      Vue.delete(state.selectedContainer || {}, 'selected');
      // store previous website state
      state.websiteHistoryUndo.push(deepClone(state.website));
      // reselect previously selected container
      Vue.set(state.selectedContainer || {}, 'selected', true);
      // if param is true, empty website history redo
      if (emptyRedo) state.websiteHistoryRedo = [];
    },
    storeStateInHistoryRedo(state) {
      // unselect selected container to avoid issues
      Vue.delete(state.selectedContainer || {}, 'selected');
      // store website state before undo
      state.websiteHistoryRedo.push(deepClone(state.website));
      // reselect previously selected container
      Vue.set(state.selectedContainer || {}, 'selected', true);
    },
    undo(state) {
      // unselect selected container to avoid issues
      state.selectedContainer = undefined;
      // set website object to a state from the undo history
      state.website = deepClone(state.websiteHistoryUndo.pop());
    },
    redo(state) {
      // unselect selected container to avoid issues
      state.selectedContainer = undefined;
      // set website object to a state from the redo history
      state.website = deepClone(state.websiteHistoryRedo.pop());
    },
    getWebsiteJson(state) {
      // unselect selected container to avoid issues
      state.selectedContainer = undefined;
      // get website json
      $.get('data/website.json').done(function(data: any) {
        // store data
        state.website = data;
        // set current page based on url path
        store.commit('goToPageFromUrl');
        // check website aspect ratio
        store.commit('checkWebsiteAspectRatio');
      }).fail(function(error: any) {
        console.log(error);
      });
    },
    loadFontsIntoWebsite(state) {
      // check compatibility
      if (document.fonts) {
        // loop through font names and load fonts into website
        for (let font of state.fonts) {
          let fontFace = new FontFace(font.split('.')[0], 'url(/fonts/' + font + ')');
          document.fonts.add(fontFace);
          fontFace.load();
        }
      }
    },
    uploadImage(state, { image }) {
      state.images.unshift(URL.createObjectURL(image));
    },
    getFileNames(state) {
      $.ajax({
        type: 'GET',
        url: 'http://localhost:9000/fileNames', // vlue server end point
        contentType: "application/json",
        dataType: "json",
        success: function(data: any) {
          console.log(data);
          // add folders and names without overwriting any existing property type data
          for (let folder in data.components) {
            if (!state.contentComponentProps[folder]) {
              Vue.set(state.contentComponentProps, folder, {});
            }
            for (let name in data.components[folder]) {
              if (!state.contentComponentProps[folder][name]) {
                Vue.set(state.contentComponentProps[folder], name, {});
              }
            }
          }
          // store image filenames
          state.images = data.images;
          // store font filenames
          state.fonts = data.fonts;
          // check compatibility
          if (document.fonts) {
            // loop through font names and load fonts into website
            for (let font of state.fonts) {
              let fontFace = new FontFace(font.split('.')[0], 'url(/fonts/' + font + ')');
              document.fonts.add(fontFace);
              fontFace.load();
            }
          }
        },
        error: function(error: any) {
          console.log(error);
        }
      })
    },
    goToPageFromUrl(state) {
      // get page name based on url path
      let urlPageName = window.location.href.split('/').pop()?.split('%20').join(' ');
      // check if url path does not match an existing page
      if (!state.website.pageContainers.find(page => page.pageName == urlPageName)) {
        // set page name to first page
        urlPageName = state.website.pageContainers[0].pageName ?? '';
      }
      // update current page
      Vue.set(state, 'currentPage', urlPageName);
    },
    checkWebsiteAspectRatio(state) {
      // calculate website height and width
      let websiteHeight = window.innerHeight;
      let websiteWidth: number;
      if (state.viewportWidth && state.viewportWidth.length) {
        websiteWidth = parseInt(state.viewportWidth.split('px')[0]);
      } else {
        websiteWidth = window.innerWidth;
        if (state.dev) websiteWidth -= 200;
      }
      // set portrait or landscape boolean flag based on website aspect ratio
      state.portraitNotLandscape = ( websiteHeight / websiteWidth > 1 );
    },
    toggleDev(state) {
      state.dev = !state.dev;
      store.commit('checkWebsiteAspectRatio');
    },
    setMode(state, mode) {
      state.mode = mode;
    },
    setSelectedLayer(state, layer) {
      state.selectedLayer = layer;
    },
    setLayerVisibility(state, layer) {
      state.layerVisibility[layer] = !state.layerVisibility[layer];
    },
    toggleEditFrame(state) {
      state.editFrame = !state.editFrame;
    },
    toggleEditPage(state) {
      state.editPage = !state.editPage;
    },
    setMobileMenuAlignment(state, { value }) {
      Vue.set(state.website.mobileMenu, 'align', value);
    },
    setMobileMenuType(state, { value }) {
      Vue.set(state.website.mobileMenu, 'type', value);
    },
    swapMobileMenuColors(state) {
      Vue.set(state.website, 'mobileMenu', {
        ...state.website.mobileMenu,
        colorA: state.website.mobileMenu.colorB,
        colorB: state.website.mobileMenu.colorA,
      });
    },
    showCreatePopup(state, params) {
      // will show create popup, and store info for it to use
      state.createPopupParams = params;
    },
    hideCreatePopup(state) {
      // will hide create popup
      state.createPopupParams = {};
    },
    showImageSelect(state, params) {
      // will show image select, and store info for it to use
      state.imageSelectParams = params;
    },
    hideImageSelect(state) {
      // will hide image select
      state.imageSelectParams = {};
    },
    showFontSelect(state, params) {
      // will show font select, and store info for it to use
      state.fontSelectParams = params;
    },
    hideFontSelect(state) {
      // will hide font select
      state.fontSelectParams = {};
    },
    setErrorMessage(state, message) {
      state.errorMessage = message;
    },
    async downloadGeneratedWebsite(state) {
      // unselect selected container to avoid issues
      Vue.delete(state.selectedContainer || {}, 'selected');
      // change state to not render dev interface
      state.websiteGeneration = true;
      state.websiteGenerationSpinner = true;
      state.dev = false;
      // store scroll position
      const scrollPosition = window.pageYOffset;
      // init zip file
      const zip = new jszip();
      const zipFolder = zip.folder("website") as jszip;
      // object used to store dynamic css, keys for css rules and values for css classes
      let cssRules = {};
      // number used to generate unique css class names
      let cssGeneratedClassName = 123456789;
      // object storing all filenames of images and fonts currently being used as keys
      let imagesToExport = {}
      let fontsToExport = {
        // default fonts
        'Abel-Pro.otf': true,
      }
      // go through all page names
      for (let pageName of store.getters.pageNames) {
        // set to next page name so that the dom renders the next page
        state.websiteGenerationPage = pageName;
        // wait until dom rerender to display the correct page without dev interface
        await Vue.nextTick();
        // get dom as html element
        let htmlElement = document.getElementById('app');
        // deep clone html element
        htmlElement = htmlElement?.cloneNode(true) as HTMLElement;
        // recursion through all elements
        function handleElement(element: Element) {
          // check if element has dynamic css applied to it
          if (element.getAttribute('style')?.length) {
            // check for image and font assets and add to image and font export objects
            let styleObject = {}
            for (let styleRow of element.getAttribute('style')?.split(';') || []) {
              if (!styleRow.trim().length) continue
              let styleAttribute = [
                styleRow.split(':')[0].trim(),
                styleRow.split(':').filter((_, i) => i > 0).join(':').trim(),
              ]
              styleObject[styleAttribute[0]] = styleAttribute[1]
            }
            // store font filename
            if (styleObject['font-family']) {
              // font css lacks filenames, so we need to find the full filename in the fonts array
              const fontFamilyWithFileType = state.fonts.find((font) => {
                return font.split('.')[0] == styleObject['font-family']
              }) || ''
              fontsToExport[fontFamilyWithFileType] = true
            }
            // store image filename
            if (styleObject['background-image']) {
              imagesToExport[styleObject['background-image']] = true
            }
            // check if the dynamic css already exists in css rules object, otherwise add it
            if (!cssRules[element.getAttribute('style') as string]) {
              cssRules[element.getAttribute('style') as string] = 'css-' + cssGeneratedClassName;
              cssGeneratedClassName = (cssGeneratedClassName * 123) % 1000000000;
            }
            // store styling in css rules object and replace with a css class
            element.classList.add(cssRules[element.getAttribute('style') as string]);
            element.removeAttribute('style');
          }
          // if the element is a nav menu link, add or remove 'selected' class
          if (element.classList.contains('vlue-content-component-nav-menu-link') ||
              element.classList.contains('vlue-mobile-menu-link')) {
            const linkPageName = element.getAttribute('href')?.split('.')[0];
            const linkPageIndex = store.getters.pageNames.indexOf(linkPageName);
            // make sure 'selected' class is applied based on the currently generated page
            if ((
                element.classList.contains('vlue-content-component-nav-menu-link') &&
                store.getters.currentAndParentPageContainerIndexes.indexOf(linkPageIndex) != -1
              ) || (
                element.classList.contains('vlue-mobile-menu-link') &&
                store.getters.currentPageContainerIndex == linkPageIndex
              )) {
              if (!element.classList.contains('selected')) element.classList.add('selected')
            } else {
              if (element.classList.contains('selected')) element.classList.remove('selected')
            }
          }
          // recursively go through child elements
          for (let child of element.children) {
            handleElement(child);
          }
        }
        // start recursing through all elements
        handleElement(htmlElement);
        // remove vue v-if artifact
        let htmlString = htmlElement?.outerHTML.split('<!---->').join('');
        // complete html
        htmlString = `
          <!DOCTYPE html>
          <html>
            <head>
              <title>Vlue Generated Website</title>
              <link rel="stylesheet" href="core.css">
              <link rel="stylesheet" href="custom.css">
              <meta charset="utf-8"/>
            </head>
            <body>
              ${htmlString}
              <script src="mobile-menu.js"></script>
            </body>
          </html>
        `;
        // add html file to zip folder
        zipFolder.file(pageName + ".html", beautify.html(htmlString));
      }
      // all pages generated, revert state
      state.websiteGeneration = false;
      state.websiteGenerationPage = undefined;
      state.dev = true;
      // wait until dom rerender with dev interface
      await Vue.nextTick();
      // apply original scroll position
      window.scrollTo(0, scrollPosition);
      // string that will be used to generate custom css file
      let customCssString = '';
      // array of promises that will be used to process images and fonts
      const getBinaryContentPromises = [] as Promise<void>[]
      // get binary data for all images to export
      const zipImagesFolder = zipFolder.folder("images") as jszip;
      for (let imageToExportName of Object.keys(imagesToExport)) {
        // add a promise that will get the binary content for this image
        getBinaryContentPromises.push(new Promise(async (resolve) => {
          // generate image file
          const data = await jszipUtils.getBinaryContent(imageToExportName
            .split('url("')[1]
            .split('")')[0]
          )
          if (!imageToExportName.includes('images/')) {
            // blob url image file
            zipImagesFolder.file('uploaded-' + imageToExportName
              .split('/')[3]
              .split('"')[0]
              + '.png'
            , data, { binary: true })
          } else {
            // images folder image file
            zipImagesFolder.file(imageToExportName
              .split('images/')[1]
              .split('")')[0]
            , data, { binary: true })
          }
          resolve()
        }))
      }
      // get binary data for all fonts to export
      const zipFontsFolder = zipFolder.folder("fonts") as jszip;
      for (let fontToExportName of Object.keys(fontsToExport)) {
        // add a promise that will get the binary content for this font
        getBinaryContentPromises.push(new Promise(async (resolve) => {
          const data = await jszipUtils.getBinaryContent('fonts/' + fontToExportName)
          zipFontsFolder.file(fontToExportName, data, { binary: true })
          resolve()
        }))
        // generate font faces for custom css file
        customCssString += '@font-face {\n  '
          + 'font-family: ' + fontToExportName.split('.')[0] + ';\n  '
          + 'src: url("fonts/' + fontToExportName + '") format("truetype");\n'
          + '}\n\n';
      }
      // generate styling rules for custom css file
      for (const css in cssRules) {
        customCssString += 'html #app .vlue-website .' + cssRules[css] + ' {\n  '
          + css.split('; ').join(';  ').split(';').map(row => {
            // convert from blob url to images folder path
            return row.includes('background-image: url("blob:')
              ? 'background-image: url("images/uploaded-'
                + row.split('/')[3].split('"')[0]
                + '.png");'
              : row;
          }).join(';\n')
          + '}\n\n';
      }
      // add custom css file to zip folder
      zipFolder.file("custom.css", customCssString);
      // get core css from css file
      let coreCssString = await $.get('core/style/core.css');
      // add core css file to zip folder
      zipFolder.file("core.css", coreCssString);
      // get mobile menu script from js file
      let mobileMenuScript = await $.get('core/scripts/mobile-menu.js', () => {}, 'text');
      // add mobile menu script to zip folder
      zipFolder.file("mobile-menu.js", mobileMenuScript);
      // add website json file to zip folder
      zipFolder.file("website.json", JSON.stringify(state.website, null, 2));
      // download zip file after all images and fonts have been processed
      await Promise.all(getBinaryContentPromises)
      const content = await zip.generateAsync({ type: "blob" })
      fileSaver.saveAs(content, "website.zip");
      // website generation done, hide spinner
      state.websiteGenerationSpinner = false;
      // reselect previously selected container
      Vue.set(state.selectedContainer || {}, 'selected', true);
    },
    async uploadGeneratedWebsite(state, { file }) {
      try {
        // get file data from zip file
        const zip = await jszip.loadAsync(file);
        // get website json from zip as string
        let websiteJsonString = await zip.files['website/website.json'].async('string');
        // find all the uploaded image files from zip file
        for (let filename of Object.keys(zip.files)) {
          if (filename.includes('website/images/uploaded-')) {
            // create blob urls and replace old blob urls with new ones in website json string
            const image = await zip.files[filename].async('blob');
            const imageUrl = URL.createObjectURL(image);
            state.images.unshift(imageUrl);
            websiteJsonString = websiteJsonString
              .split('blob:'
                + location.origin + '/'
                + filename.split('-').filter((_, i) => i > 0).join('-').split('.')[0]
              ).join(imageUrl);
          }
        }
        // replace old website state with new website state from zip file
        state.website = JSON.parse(websiteJsonString);
      } catch (error) {
        state.errorMessage = {
          title: 'Invalid ZIP file',
          description: 'Only upload ZIP files previously generated by the "Download To ZIP" feature'
        };
      }
    },
    async uploadVueComponent(_, { file }) {
      // not working, unsure if the thing is even doable, abandoning feature
      Vue.component(
        'custom-component',
        // this needs to be an options object not a single file component string
        // also this won't work because it comes after creating the root vue instance
        await $.get(URL.createObjectURL(file), () => {}, 'text'),
      );
    },
    cleanSlate(state) {
      // unselect selected container to avoid issues
      state.selectedContainer = undefined;
      // clean slate
      state.website = deepClone(cleanSlateJson);
      // set current page
      state.currentPage = state.website.pageContainers[0].pageName ?? '';
    },
    saveChanges(state) {
      // unselect selected container to avoid issues
      Vue.delete(state.selectedContainer || {}, 'selected');
      // call to vlue server to save changes to local json file
      $.ajax({
        type: 'POST',
        url: 'http://localhost:9000/saveChanges', // vlue server end point
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify(state.website),
        success: function(data: any) {
          console.log(data);
        },
        error: function(error: any) {
          console.log(error);
        }
      });
      // reselect previously selected container
      Vue.set(state.selectedContainer || {}, 'selected', true);
    },
    resetViewportWidth(state) {
      state.viewportWidth = '';
      store.commit('checkWebsiteAspectRatio');
    },
    moveContainer(state, { current, currentVueObject, direction }) {
      // make sure you are not trying to move a container into one of its child containers
      let visitedVueObject = currentVueObject;
      let illegalOperation = false;
      while (visitedVueObject.$parent) {
        if (visitedVueObject.$parent.props?.selected) illegalOperation = true;
        visitedVueObject = visitedVueObject.$parent;
      }
      // if no container is selected or if the clicked container is selected
      if (!state.selectedContainer || current.selected || illegalOperation) {
        // select or unselect clicked container
        store.commit('selectContainer', { current, currentVueObject });
        return;
      }
      // act as if you clicked the down button to avoid replacing existing content
      if (direction === 'center' && current.contentComponents) direction = 'down';
      // add a new container inside the clicked container
      if (direction !== 'center') {
        store.commit('addContainer', {
          current,
          direction,
          size: '',
        });
      }
      // create a deep clone of the previously selected container
      const selectedContainerClone = deepClone(state.selectedContainer);
      // remove the previously selected container
      store.commit('removeContainer', {
        moving: true,
        current: state.selectedContainer,
        parent: state.selectedContainerVueObject?.$parent,
        index: state.selectedContainerVueObject?.$parent.props.childContainers
          .findIndex((child: Container) => child.selected),
      });
      // set the new container to become the container that was previously selected
      if (direction === 'center') {
        Vue.set(
          currentVueObject.$parent.props.childContainers,
          currentVueObject.$props.index,
          selectedContainerClone,
        );
      } else {
        Vue.set(
          current.childContainers,
          ("left up").includes(direction) ? 0 : current.childContainers.length - 1,
          selectedContainerClone,
        );
      }
      // unselect the new container
      store.commit('selectContainer', { current: selectedContainerClone });
    },
    addContainer(_, { current, direction, size }) {
      // check if current container needs to be nested before operation
      if (current.align == 'column' && ("left right").includes(direction)
      ||  current.align == 'row'    && ("up down"   ).includes(direction)
      ||  current.align == 'grid'
      || !current.childContainers
      ||  current.contentComponents) {
        // nest current container in new child container with current values
        Vue.set(current, 'childContainers', [deepClone(current)]);
        // loop through all container props
        for (let prop in current) {
          // delete duplicate props
          Vue.delete(
            // do nothing, handled above
            ['childContainers'             ].indexOf(prop) > -1 ? {} :
            // these props are exceptions that should only be on current container
            ['selected', 'size', 'pageName'].indexOf(prop) > -1 ? current.childContainers[0] :
            // all other props should only be on child container
            current,
            // prop to be deleted
            prop,
          );
        }
      }
      // set align value based on which button was pressed
      Vue.set(current, 'align', ("up down").includes(direction) ? 'column' : 'row');
      // push or unshift depending on which button was pressed
      current.childContainers[("right down").includes(direction) ? "push" : "unshift"](
        // set size value of new child container based on which option was chosen
        (size.includes('px') || size.includes('%'))
        ? { size }
        : {}
      );
    },
    addGridContainers(_, { current, grid }) {
      // set align value to grid
      Vue.set(current, 'align', 'grid');
      // set size of child containers based on grid option chosen
      Vue.set(current, 'gridCellSize', grid.pixelsPerCell + 'px');
      // create child containers array
      Vue.set(current, 'childContainers',
        // create number of child containers based on grid option chosen
        deepClone(Array(parseInt(grid.numberOfCells)).fill({}))
      );
    },
    addContentComponent(_, { current, folder, name }) {
      if (!current.contentComponents) {
        // if container has no content components
        // add content component to clicked container
        Vue.set(current, 'contentComponents', [{
          pathway: { folder, name },
          props: {},
        }]);
        // select clicked container if not already selected
        if (!current.selected) store.commit('selectContainer', { current });
      } else {
        // if container already has a content component
        // divide container into two child containers
        store.commit('addContainer', {
          current,
          direction: 'down',
          size: 'fit-content',
        });
        // add content component to new child container
        Vue.set(current.childContainers[1], 'contentComponents', [{
          pathway: { folder, name },
          props: {},
        }]);
        // select new child container
        store.commit('selectContainer', { current: current.childContainers[1] });
      }
    },
    addPage(state) {
      // find a unique page name number to avoid duplicate page names
      let pageNameNumber = state.website.pageContainers.length + 1;
      while (state.website.pageContainers.find(page =>
        page.pageName == 'Page ' + pageNameNumber
      )) {
        pageNameNumber ++;
      }
      // add a new page container with a name based on page number
      state.website.pageContainers.push({ pageName: 'Page ' + pageNameNumber });
    },
    sortPage(state, { direction, getters }) {
      // swap indexes of two page containers
      const index = getters.currentPageContainerIndex - (direction == 'up' ? 1 : 0);
      const pages = state.website.pageContainers;
      pages.splice(index, 2, pages[index + 1], pages[index]);
    },
    sortContentComponent(state, { direction, index }) {
      // swap indexes of two content components
      index -= (direction == 'up' ? 1 : 0);
      const components = state.selectedContainer?.contentComponents;
      components?.splice(index, 2, components[index + 1], components[index]);
    },
    selectContainer(state, { current, currentVueObject }) {
      // stop displaying image and font select menus to avoid confusion
      state.imageSelectParams = {};
      state.fontSelectParams = {};
      // update selected boolean and selected container reference
      if (current.selected) {
        // unselect clicked container
        Vue.delete(current, 'selected');
        state.selectedContainer = undefined;
        state.selectedContainerVueObject = undefined;
      } else {
        // select clicked container and unselect previously selected container
        Vue.delete(state.selectedContainer || {}, 'selected');
        Vue.set(current, 'selected', true);
        state.selectedContainer = current;
        state.selectedContainerVueObject = currentVueObject;
      }
    },
    setProperty(state, { current, property, value, global }) {
      // edge case for when switching between pages
      if (property.name == 'currentPage') {
        // update url path to current page name
        window.history.pushState('', '', '/' + value);
        // update current page
        Vue.set(state, 'currentPage', value);
        // current page update handled, end function
        return
      }
      // edge case for when updating a page name
      if (property.name == 'pageName') {
        // prevent setting two pages to the same page name
        if (state.website.pageContainers.find(page => page.pageName == value)) return;
        // update the current page property
        state.currentPage = value;
      }
      // if no object or global path has been sent in, apply property change to state object
      if (!current && !global) current = state;
      // if global path has been sent in, apply property change to global property object
      if (global) {
        if (!state.website.globalComponentPropValues[global.folder]) {
          Vue.set(state.website.globalComponentPropValues, global.folder, {});
        }
        if (!state.website.globalComponentPropValues[global.folder][global.name]) {
          Vue.set(state.website.globalComponentPropValues[global.folder], global.name, {});
        }
        current = state.website.globalComponentPropValues[global.folder][global.name];
      }
      // make sure viewport width value ends with px instead of being just a number
      if (property.name == 'viewportWidth' && !isNaN(value)) value = value + 'px';
      // set property to new value, or delete property if no value is provided
      (value === true || value.trim().length)
      ? Vue.set(   current, property.name, value)
      : Vue.delete(current, property.name);
      // check website aspect ratio
      if (property.name == 'viewportWidth') store.commit('checkWebsiteAspectRatio');
    },
    componentPropertySetup(state, { pathway, props }) {
      // add folder if it's not already present
      if (!state.contentComponentProps[pathway.folder]) {
        Vue.set(state.contentComponentProps, pathway.folder, {});
      }
      // add name if it's not already present
      if (!state.contentComponentProps[pathway.folder][pathway.name]) {
        Vue.set(state.contentComponentProps[pathway.folder], pathway.name, {});
      }
      // check if content component property types have been stored in state object
      if (!Object.keys(state.contentComponentProps[pathway.folder][pathway.name]).length) {
        // go through all props
        for (let prop of props) {
          // store content component property types and default values in state object
          // image and font type props are a special case
          // set default value to first image or font in images or fonts folder
          Vue.set(state.contentComponentProps[pathway.folder][pathway.name], prop.name, {
            type    : prop.type,
            default : prop.type == 'image' ? state.images[0]
                    : prop.type == 'font'  ? state.fonts[0]
                    : prop.value,
          });
        }
      }
    },
    removePage(state, { getters }) {
      // remove current page from page containers
      state.website.pageContainers.splice(getters.currentPageContainerIndex, 1);
      // reset current page name property
      state.currentPage = state.website.pageContainers[0].pageName ?? '';
    },
    removeContainer(state, { current, parent, index, moving }) {
      // returns true if page container exists somewhere in the hierarchy of a given container
      function containsPageContainer(container: Container) {
        if (container.pageContainers) return true;
        if (container.childContainers) {
          for (let child of container.childContainers) {
            if (containsPageContainer(child)) return true;
          }
        }
        return false;
      }
      if (
        // layer containers and page containers are not allowed to be removed
        ( parent.props && !containsPageContainer(current) && !parent.props.pageContainers )
        // if we are removing a container but adding a clone of it elsewhere, be less strict
        || moving
      ) {
        // remove container
        parent.props.childContainers.splice(index, 1);
        // if there is only one child container left we will remove redundant nesting
        if (parent.props.childContainers.length == 1) {
          // redundantly nested child container
          let child = parent.props.childContainers[0];
          // make sure parent size and page name is retained by applying them to child
          child.size     = parent.props.size;
          child.pageName = parent.props.pageName;
          // check type of grandparent parent relationship and replace parent with child
          let grandparent = parent.$parent.props || {};
          grandparent.childContainers
          ? grandparent.childContainers.splice(parent.index, 1, child)
          : grandparent.pageContainers
          ? state.website.pageContainers.splice(parent.index, 1, child)
          : Vue.set(state.website, state.selectedLayer + 'LayerContainer', child);
        }
      }
    },
    removeContentComponent(state, { index }) {
      // remove content component from container
      state.selectedContainer?.contentComponents?.splice(index, 1);
      // if content component array is empty, remove array from container
      if (!state.selectedContainer?.contentComponents?.length) {
        Vue.delete(state.selectedContainer || {}, 'contentComponents');
      }
    },
  },
  getters: {
    pageNames(state) {
      return (state.website.pageContainers || [])
        .map(page => page.pageName);
    },
    parentPageNames(state) {
      return (state.website.pageContainers || [])
        .filter(page => !page.parentPageName)
        .map(page => page.pageName);
    },
    pageNamesWithParentPageNames(state) {
      return (state.website.pageContainers || [])
        .map(page => ({
          pageName: page.pageName,
          parentPageName: page.parentPageName,
        }));
    },
    currentPageContainer(state) {
      return (state.website.pageContainers || [])
        .find(page => {
          return page.pageName == (state.websiteGenerationPage
            ? state.websiteGenerationPage
            : state.currentPage)
        });
    },
    currentPageContainerIndex(state) {
      const currentPage = state.websiteGenerationPage || state.currentPage
      for (const [index, page] of (state.website.pageContainers || []).entries()) {
        if (page.pageName == currentPage) return index;
      }
    },
    currentAndParentPageContainerIndexes(state) {
      const currentPage = state.websiteGenerationPage || state.currentPage
      // find and return indexes of current page and its parent page
      let currentPageParentPageName: string | undefined;
      for (const [_, page] of (state.website.pageContainers || []).entries()) {
        if (currentPage == page.pageName) currentPageParentPageName = page.parentPageName;
      }
      let indexes = [] as number[];
      for (const [index, page] of (state.website.pageContainers || []).entries()) {
        if (page.pageName == currentPage
        ||  page.pageName == currentPageParentPageName) {
          indexes.push(index);
        }
      }
      return indexes;
    },
  }
})

function deepClone(object: any) {
  return JSON.parse(JSON.stringify(object));
}

window.onpopstate = function() {
  store.commit('goToPageFromUrl');
}

window.onresize = function() {
  store.commit('checkWebsiteAspectRatio');
}

export default store
