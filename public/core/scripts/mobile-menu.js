// expand and collapse mobile menu when clicking the mobile menu button
document.getElementsByClassName('vlue-mobile-menu-button')[0]
  .addEventListener('click', function() {
    document.getElementsByClassName('vlue-mobile-menu-container')[0]
      .classList.toggle('expanded')
  }
)

// expand and collapse list of child pages when clicking the parent page arrow button
var mobileMenuArrowButtons = document
  .getElementsByClassName('vlue-mobile-menu-link-expand-arrow')
for (let arrowButton of mobileMenuArrowButtons) {
  arrowButton.addEventListener('click', function(event) {
    event.target.parentNode.parentNode.classList.toggle('expanded')
  })
}
